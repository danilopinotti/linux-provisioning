#!/bin/bash
WORKDIR=$(pwd)

welcome() {
	echo "Executando script de provisionamento desenvolvido por Danilo Pinotti"
}

is_root(){
	if [[ ! $EUID -eq 0 ]]; then
		exec sudo $0 $@ || echo "Must be run as root user"
		exit 1 # Fail Sudo
	fi
}

configure_bashrc(){
	echo -ne "Configuring bashrc ... "

	for i in $(find /home -maxdepth 1 -type d); do
		file="$i/.bashrc"
		if [ -f "$file" ]
		then
			echo "echo '  _    _ _______ ______ _____  _____  '" >> $file;
			echo "echo ' | |  | |__   __|  ____|  __ \|  __ \ '" >> $file;
			echo "echo ' | |  | |  | |  | |__  | |__) | |__) |'" >> $file;
			echo "echo ' | |  | |  | |  |  __| |  ___/|  _  / '" >> $file;
			echo "echo ' | |__| |  | |  | |    | |    | | \ \ '" >> $file;
			echo "echo '  \____/   |_|  |_|    |_|    |_|  \_\'" >> $file;
			echo "echo ''" >> $file;
			echo "echo ' Para ter acesso root:'" >> $file;
			echo "echo ' Comando: sudo su'" >> $file;
			echo "echo ''" >> $file;
		fi
	done

	echo "[OK]"
}

configure_crontab(){
	CRONTAB_FILE=/etc/crontab
	echo -ne 'Configuring crontab ... '

	sed -i.bak '/^[#[:space:]\n ].*$/d' $CRONTAB_FILE
	echo '00 00 * * * root shutdown -h now' >> $CRONTAB_FILE

	echo 'OK'
}

remove_bash_history(){
	echo -ne "Clearing bash history ... "
	echo "" > /root/.bash_history
	echo "" > /home/utfpr/.bash_history
	echo "[OK]"
}

configure_utc(){
	echo -ne "Configuring UTC ... "

	if [[ $(lsb_release -sc) == "xenial" ]]; then
		timedatectl set-local-rtc 1
	else
		sudo sed -i.bak 's/UTC=yes/UTC=no/' /etc/default/rcS
	fi

	echo "[OK]"
}

clear_system(){
	remove_bash_history
}

run_provisioning() {
	welcome
	is_root
	configure_bashrc
	configure_crontab
	configure_utc
	clear_system
}

run_provisioning
