#!/bin/bash
if [[ ! $EUID -eq 0 ]]; then
   	exec sudo $0 $@ || echo "Must be run as root user"
   	exit 1 # Fail Sudo
fi

./provisioning.sh
./installations.sh
./join_domain.sh