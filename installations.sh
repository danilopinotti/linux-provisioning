#!/bin/bash

repositories=(
	"ppa:danielrichter2007/grub-customizer" 
)

softwares=(
	"git" 
	"build-essential" 
)

#################################################################################################

command_status(){
	if [ $? -eq 0 ]; then
	    echo "[OK]"
	else
	    echo "[FAIL]"
	fi
}

third_party_repositories(){
	for i in ${!repositories[@]};
	do
		echo -ne "Configuring repository "${repositories[$i]}" ... "
		apt-add-repository ${repositories[$i]} -y 2> /dev/null > /dev/null
		command_status
	done
}

install_softwares(){
	local flag=0

	for i in ${!softwares[@]};
	do
		echo -ne "Installing software: "${softwares[$i]}" ... "
		apt-get install ${softwares[$i]} -y -q=2 2> /dev/null > /dev/null
		if [ $? -eq 0 ]; then
	    	echo "[OK]"
		else
		    echo "[FAIL]"
		    
		    resolve_dependencies

		    echo -ne "Installing software (again) : "${softwares[$i]}" ... "
			apt-get install ${softwares[$i]} -y -q=2 2> /dev/null > /dev/null
			command_status
		fi
	done
}

update_lists(){
	echo -ne "Updating lists ... "
	apt-get update > /dev/null
	command_status
}

is_root(){
	if [[ ! $EUID -eq 0 ]]; then
    	exec sudo $0 $@ || echo "Must be run as root user"
    	exit 1 # Fail Sudo
	fi
}

resolve_dependencies(){
	echo -ne "Trying to resolve dependencies ... "
	apt-get install -f -qq > /dev/null
	command_status
}

autoclean_autoremove(){
	echo -ne "Executing autoremove and autoclean ... "

	apt-get autoremove -y > /dev/null
	apt-get autoclean -y > /dev/null

	command_status
}

upgrade_softwares(){
	echo -ne "Upgrading softwares ... "

	apt-get upgrade -y > /dev/null

	command_status
}

run_installations(){
	is_root
	third_party_repositories
	update_lists
	install_softwares
	#upgrade_softwares
	autoclean_autoremove
}

run_installations