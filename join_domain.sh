#!/bin/bash 
HOSTNAME=$(cat /etc/hostname)
TEMP_FILE="./host_modified"
NTP_SERVER="172.20.10.245"
WORKDIR=$(pwd)
BACKUP_FOLDER=$(pwd)
USERNAME='fog-gp'
PASSWORD='F7xbBzLv'

STEP=1

newStep(){
	echo "Passo "$STEP": "
	STEP=$(($STEP+1))
}

backupConfig() {
	echo "- Criando backup de "$1
	cp $1 $WORKDIR"/"$(basename $1)".bkp"

	if [ -f "$1.bkp" ]
	then
		echo "-- Backup $1.bkp já existe."
	else
		cp $1 $1".bkp"
		echo "-- Backup criado em: $1.bkp"
	fi

	cp $1 $BACKUP_FOLDER"/"$(basename $1)".bkp"

	echo "-- Backup criado em:" $WORKDIR"/"$(basename $1)".bkp"
}

updateHosts () {
	newStep	
	echo "Configurando /etc/hosts ... "
	backupConfig /etc/hosts

	$(cp /etc/hosts $TEMP_FILE)

	sed -i -E '/127\.0\.0\.1([[:space:]]*)localhost/a 172\.20\.10\.247	sambapdc' $TEMP_FILE
	sed -i -E 's/(127\.0\.0\.1[[:space:]]*)localhost/\1'$HOSTNAME'.ad.gp.utfpr.edu.br	localhost	'$HOSTNAME'/' $TEMP_FILE
	mv $TEMP_FILE /etc/hosts
	rm -f $TEMP_FILE 
	echo "[OK]"
}

installPackages () {
	newStep	
	echo "Atualizando pacotes ... "
	apt-get update -q=2 -y > /dev/null 2> /dev/null
	echo "[OK]"

	newStep
	echo "Instalando pacotes ... "
	apt-get install -y -yy -q=2 \
			krb5-user krb5-config \
			winbind \
			samba samba-common smbclient \
			cifs-utils \
			libpam-krb5 libpam-winbind libnss-winbind \
			ntp > /dev/null 2> /dev/null
	echo "[OK]"
}

configureNTP () {
	newStep	
	echo "Configurando /etc/ntp.conf"
	backupConfig /etc/ntp.conf

	if [ $(egrep -c $NTP_SERVER /etc/ntp.conf) -ne 0 ]
	then
		echo "NTP ja está configurado."
	else
		echo "server "$NTP_SERVER >> /etc/ntp.conf
		echo "restrict "$NTP_SERVER >> /etc/ntp.conf
		service ntp restart
		
	fi
	echo "[OK]"
}

configureKerberos () {
	newStep	
	echo "Configurando /etc/krb5.conf"
	backupConfig /etc/krb5.conf	

	echo "[libdefaults]
default_realm = AD.GP.UTFPR.EDU.BR

[realms]
AD.GP.UTFPR.EDU.BR  =  {
	kdc  =  sambapdc.ad.gp.utfpr.edu.br
	default_domain  =  UTFPR-GP
	admin_server  =  sambapdc.ad.gp.utfpr.edu.br
} 

[domain_realm]
.ad.gp.utfpr.edu.br = AD.GP.UTFPR.EDU.BR" > /etc/krb5.conf
	echo "[OK]"

}

testKerberos () {
	newStep	
	echo "Testanto Kerberos"

	#echo "Insira seu usuário do domínio"


	echo $PASSWORD | kinit $USERNAME

	echo "Saída \$ klist "
	klist
	echo "[OK]"
}

configureSamba () {
	newStep	
	echo "Configurando /etc/samba/smb.conf"
	backupConfig /etc/samba/smb.conf

	echo "[global]
security = ads 
realm= ad.gp.utfpr.edu.br 
workgroup = UTFPR-GP 
idmap uid = 10000-15000 
idmap gid = 10000-15000 
winbind enum users = yes 
winbind enum groups = yes 
template homedir = /home/%D/%U 
template shell = /bin/bash 
client use spnego = yes 
winbind use default domain = yes 
restrict anonymous = 2 
winbind refresh tickets = yes 
" > /etc/samba/smb.conf


	#Reiniciando serviços
	service winbind restart
	service smbd restart
	service nmbd restart

	echo "[OK]"
}

joinDomain () {
	newStep	
	echo "Inserindo no domínio"

	#echo "Insira seu usuário do domínio"
	#read USERNAME	

	echo $PASSWORD | net ads join -U $USERNAME
	echo "[OK]"
}	

configureCommonSession () {
	newStep	
	echo "Configurando /etc/pam.d/common-session"
	backupConfig /etc/pam.d/common-session

	sed -i -E '/([[:space:]]+)pam_unix\.so/a session required pam_mkhomedir.so umask=0022 skel=/etc/skel' /etc/pam.d/common-session
	echo "[OK]"
}

configureNsSwitch () {
	newStep	
	echo "Configurando /etc/nsswitch.conf" 
	backupConfig /etc/nsswitch.conf 

	sed -i -E 's/passwd:([[:space:]]+)compat/passwd: compat winbind/' /etc/nsswitch.conf 
	sed -i -E 's/group:([[:space:]]+)compat/group: compat winbind/' /etc/nsswitch.conf

	echo "[OK]"
}

configureLightdm() {
	newStep	
	echo "Configurando /etc/lightdm/lightdm.conf"

	if [ -f "/etc/lightdm/lightdm.conf" ]
	then
		echo "Arquivo encontrado."
		backupConfig /etc/lightdm/lightdm.conf
	else
		echo "Arquivo não encontrado."
		echo "Criando o arquivo."
		touch /etc/lightdm/lightdm.conf
	fi

	echo "Definindo configurações"

	echo "[SeatDefaults]
allow-guest=false
greeter-show-manual-login=true
greeter-hide-users=true" >> /etc/lightdm/lightdm.conf

	echo "[OK]"

}

configureResolv (){
	newStep	
	echo "Configurando /etc/lightdm/lightdm.conf"
	backupConfig /etc/resolv.conf 

	echo "nameserver 172.20.10.247" > /etc/resolv.conf

	echo "[OK]"

}

setAllUsersRoot(){
	newStep	
	echo "Setando root"
	backupConfig /etc/sudoers 
	
	echo '%domain\ users ALL=NOPASSWD:ALL' >> /etc/sudoers

	echo '[OK]'
}

run_join_domain () {
	updateHosts
	configureResolv
	installPackages
	configureNTP
	configureKerberos
	testKerberos
	configureSamba
	joinDomain
	configureNsSwitch
	configureCommonSession
	configureLightdm
	setAllUsersRoot
}

run_join_domain
